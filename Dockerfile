FROM nginx:alpine3.18-slim

COPY index.html /usr/share/nginx/html

EXPOSE 80


# run nginx in forground
CMD ["nginx", "-g", "daemon off;"]

